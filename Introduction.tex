% -*- latex -*-

\chapter{Introduction}
\label{chap:Introduction}

High-performance computing relies on ever finer threading. Advances in
processor technology include ever greater numbers of cores, hyperthreading,
accelerators with integrated blocks of cores, and special vectorized
instructions, all of which require more software parallelism to achieve
peak performance. Traditional visualization solutions cannot support this
extreme level of concurrency. Extreme scale systems require a new
programming model and a fundamental change in how we design algorithms. To
address these issues we created \VTKm: the visualization toolkit for
multi-/many-core architectures.

\VTKm supports a number of algorithms and the ability to design further
algorithms through a top-down design with an emphasis on extreme
parallelism. \VTKm also provides support for finding and building links
across topologies, making it possible to perform operations that determine
manifold surfaces, interpolate generated values, and find adjacencies.
Although \VTKm provides a simplified high-level interface for programming,
its template-based code removes the overhead of abstraction.

\begin{figure}[htb]
  \centering
  %% \begin{tabular}{ccc}
  %%   CUDA SDK & PISTON & \VTKm \\
  %%   {\small 431 LOC} & {\small 369 LOC} & {\small 265 LOC} \\
  %%   \includegraphics[width=.75in]{images/MCCompareCuda} &
  %%   \includegraphics[width=.75in]{images/MCComparePiston} &
  %%   \includegraphics[width=.75in]{images/MCCompareVTKm}
  %% \end{tabular}
  \begin{tabular}{cc}
    CUDA SDK & \VTKm \\
    {\small 431 LOC} & {\small 265 LOC} \\
    \includegraphics[width=.75in]{images/MCCompareCuda} &
    \includegraphics[width=.75in]{images/MCCompareVTKm}
  \end{tabular}
  \caption[Comparison of Marching Cubes implementations.]{
    Comparison of the Marching Cubes algorithm in \VTKm and the reference implementation in the CUDA SDK.
    Implementations in \VTKm are simpler, shorter, more general, and easier to maintain.
    (Lines of code (LOC) measurements come from cloc.)}
  \label{fig:MCCompare}
\end{figure}

\VTKm simplifies the development of parallel scientific visualization algorithms by providing a framework of supporting functionality that allows developers to focus on visualization operations.
Consider the listings in Figure~\ref{fig:MCCompare} that compares the size of the implementation for the Marching Cubes algorithm in \VTKm with the equivalent reference implementation in the CUDA software development kit.
Because \VTKm internally manages the parallel distribution of work and data, the \VTKm implementation is shorter and easier to maintain.
Additionally, \VTKm provides data abstractions not provided by other libraries that make code written in \VTKm more versatile.

%% \begin{didyouknow}
%%   \VTKm is written in C++ and makes extensive use of templates. The toolkit
%%   is implemented as a header library, meaning that all the code is
%%   implemented in header files (with extension \textfilename{.h}) and
%%   completely included in any code that uses it. This allows the compiler to
%%   inline and specialize code for better performance.
%% \end{didyouknow}

\section{How to Use This Guide}

This user's guide is organized into 5 parts to help guide novice to advanced users and to provide a convenient reference.
Part \ref{part:GettingStarted}, Getting Started, provides a brief overview of using \VTKm.
This part provides instructions on building \VTKm and some simple examples of using \VTKm.
Users new to \VTKm are well served to read through Part \ref{part:GettingStarted} first to become acquainted with the basic concepts.

The remaining parts, which provide detailed documentation of increasing complexity, have chapters that do not need to be read in detail.
Readers will likely find it useful to skip to specific topics of interest.

Part \ref{part:Using}, Using \VTKm, dives deeper into the \VTKm library.
It provides much more detail on the concepts introduced in Part \ref{part:GettingStarted} and introduces new topics helpful to people who use \VTKm's existing algorithms.

Part \ref{part:Developing}, Developing Algorithms, documents how to use \VTKm's framework to develop new or custom visualization algorithms.
In this part we dive into the inner workings of filters and introduce the concept of a \keyterm{worklet}, which is the base unit used to write a device-portable algorithm in \VTKm.
Part \ref{part:Developing} also documents many supporting functions that are helpful in implementing visualization algorithms.

Part \ref{part:Advanced}, Advanced Development, explores in more detail how \VTKm manages memory and devices.
This information describes how to adapt \VTKm to custom data structures and new devices.

Part \ref{part:Core}, Core Development, exposes the inner workings of \VTKm.
These concepts allow you to design new algorithmic structures not already available in \VTKm.

\section{Conventions Used in This Guide}

When documenting the \VTKm API, the following conventions are used.
\begin{itemize}
\item Filenames are printed in a \textfilename{sans serif font}.
\item C++ code is printed in a \textcode{monospace font}.
\item Macros and namespaces from \VTKm are printed in \textnamespace{red}.
\item Identifiers from \VTKm are printed in \textidentifier{blue}.
\item Signatures, described in Chapter \ref{chap:SimpleWorklets}, and the
  tags used in them are printed in \textsignature{green}.
\end{itemize}

This guide provides actual code samples throughout its discussions to
demonstrate their use. These examples are all valid code that can be
compiled and used although it is often the case that code snippets are
provided. In such cases, the code must be placed in a larger context.

\begin{didyouknow}
  In this guide we periodically use these \textbf{Did you know?} boxes to
  provide additional information related to the topic at hand.
\end{didyouknow}

\begin{commonerrors}
  \textbf{Common Errors} blocks are used to highlight some of the common
  problems or complications you might encounter when dealing with the topic
  of discussion.
\end{commonerrors}
