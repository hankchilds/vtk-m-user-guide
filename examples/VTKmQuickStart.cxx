////
//// BEGIN-EXAMPLE VTKmQuickStart.cxx
////
#include <vtkm/cont/Initialize.h>

#include <vtkm/io/reader/VTKDataSetReader.h>

#include <vtkm/filter/PointElevation.h>

#include <vtkm/rendering/Actor.h>
#include <vtkm/rendering/CanvasRayTracer.h>
#include <vtkm/rendering/MapperRayTracer.h>
#include <vtkm/rendering/Scene.h>
#include <vtkm/rendering/View3D.h>

////
//// BEGIN-EXAMPLE VTKmQuickStartInitialize.cxx
////
int main(int argc, char* argv[])
{
  vtkm::cont::Initialize(argc, argv);
  ////
  //// END-EXAMPLE VTKmQuickStartInitialize.cxx
  ////

  if (argc != 2)
  {
    std::cerr << "USAGE: " << argv[0] << " <file.vtk>" << std::endl;
    return 1;
  }

  // Read in a file specified in the first command line argument.
  ////
  //// BEGIN-EXAMPLE VTKmQuickStartReadFile.cxx
  ////
  vtkm::io::reader::VTKDataSetReader reader(argv[1]);
  vtkm::cont::DataSet inData = reader.ReadDataSet();
  ////
  //// END-EXAMPLE VTKmQuickStartReadFile.cxx
  ////
  //// PAUSE-EXAMPLE
  inData.PrintSummary(std::cout);
  //// RESUME-EXAMPLE

  // Run the data through the elevation filter.
  ////
  //// BEGIN-EXAMPLE VTKmQuickStartFilter.cxx
  ////
  vtkm::filter::PointElevation elevation;
  elevation.SetUseCoordinateSystemAsField(true);
  elevation.SetOutputFieldName("elevation");
  vtkm::cont::DataSet outData = elevation.Execute(inData);
  ////
  //// END-EXAMPLE VTKmQuickStartFilter.cxx
  ////

  // Render an image and write it out to a file.
  ////
  //// BEGIN-EXAMPLE VTKmQuickStartRender.cxx
  ////
  //// LABEL scene-start
  vtkm::rendering::Actor actor(outData.GetCellSet(),
                               outData.GetCoordinateSystem(),
                               outData.GetField("elevation"));

  vtkm::rendering::Scene scene;
  //// LABEL scene-end
  scene.AddActor(actor);

  vtkm::rendering::MapperRayTracer mapper;

  vtkm::rendering::CanvasRayTracer canvas(1280, 1024);

  //// LABEL view
  vtkm::rendering::View3D view(scene, mapper, canvas);
  //// LABEL view-initialize
  view.Initialize();

  //// LABEL paint
  view.Paint();

  //// LABEL save
  view.SaveAs("image.ppm");
  ////
  //// END-EXAMPLE VTKmQuickStartRender.cxx
  ////

  return 0;
}
////
//// END-EXAMPLE VTKmQuickStart.cxx
////
